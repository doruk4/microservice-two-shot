from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
    ]


class ShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin_vo_id=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID!"},
                status=400
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_shoes(request, pk):
    if request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
