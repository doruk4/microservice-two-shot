# Wardrobify

Team:

* Person 1 - Doruk is doing Hats
* Person 2 - Matthew is doing Shoes

## To Run:
You will need Docker Desktop installed.
After cloning the repository, enter your terminal then:

1. Navigate to the root directory of the repository

1. Enter the following:

- docker volume create two-shot-pgdata
- docker-compose build
- docker-compose up

3. The page for Wardrobify will be able to be accessed on http://localhost:3000/

### Creating Shoes:

Before creating shoes, you must create a Bin:

Using a program like Insomnia or Postman, you will need to make a bin by sending a "POST" request to http://localhost:8100/api/bins/ with the following JSON object format:

    {
        "closet_name": "Your Closet Name"
        "bin_number": PositiveSmallIntergerField ex: 5
        "bin_size": PositiveSmallIntergerField ex: 10
    }

After that, your bin will appear in the "Choose a Bin" dropdown in the New Shoes form.


## Design

## Shoes microservice

![Excalidraw of Shoes Microservice](img/Wardrobify_Shoes.png)


Shoes RESTful API has two models, the "Shoes" model, and the "BinVO" model:

The Shoes model has the following fields:

- manufacturer
- name
- color
- picture_url
- bin (foreign key)

BinVo:

- import_href
- closet_name

BinVo pulls data from from the wardrobe API by way of a polling service, which checks for new bins from the wardrobe API.




## Hats microservice

My model takes in all the necessary descriptors listed in the project specifications as well as an encoder for the Location data which I'm pulling for the wardrobe api.
I created 3 pages-
    1. A list page that has all the hats in the database, organized in columns and displaying the picture as well as color and style. As well as a delete button. Clicking on the image will redirect to a specific detail page.
    2. A detail page which has all the details specified in the project document.Can also delete from here.
    3. A create hat form to create a new hat.
