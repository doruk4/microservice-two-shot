import {useParams} from 'react-router-dom';
import {useEffect, useState} from 'react';



const HatDetail = () => {

    const [hat, setHat] = useState();
    const { id } = useParams()
        const fetchData = async () => {
            const url = `http://localhost:8090/hats/${id}`;
            console.log(url)
            try {
                const response = await fetch(url);

                if (response.ok) {
                    const data = await response.json();
                    setHat(data);

                    console.log(data)
                    console.log(data.fabric)
                } else {
                    throw new Error('Network response was not ok');
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        useEffect(() => {
        fetchData();
    }, [id]);
    const deleteHat = async (id) => {
        const hatUrl = `http://localhost:8090/hats/${id}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            window.location.reload()
        }
      }
    if (hat){
    return (
        <div>

                <div>
                    <img src= {hat.pict_url} alt="Hat" />
                    <ul>
                        <li>
                            <p>Fabric: {hat.fabric}</p>
                        </li>
                        <li>
                            <p>Style: {hat.style_name}</p>
                        </li>
                        <li>
                            <p>Color: {hat.color}</p>
                        </li>
                        <li>
                            <p>Location: {hat.location.closet_name}</p>
                        </li>
                        <button onClick={(e) => deleteHat(hat.id)} >Delete</button>
                    </ul>
                </div>

        </div>
    );}


            }

export default HatDetail;
