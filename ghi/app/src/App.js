import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useState, useEffect } from 'react';
import HatsList from './HatsList.js';
import HatForm from './HatForm.js'
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';

import HatDetail from './HatDetail.js'

function App() {
  const [hats, setHats] = useState([])
  const [shoes, setShoes] = useState([])

  async function loadHats() {
    const response = await fetch('http://localhost:8090/hats/');
    const { hats } = await response.json();
    setHats(hats)
  }



  useEffect(() => {
    loadHats()
    getShoes();
  }, [])


  async function getShoes() {
    const url = "http://localhost:8080/api/shoes"
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setShoes(data.shoes)
    }
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList shoes={shoes} />} />
          <Route path="shoes/new" element={<ShoesForm getShoes={getShoes} />} />
          <Route path='hats'>
            <Route path='list' element={<HatsList hats={hats} />}/>
            <Route path='new' element={<HatForm />}/>
            <Route path=':id' element={<HatDetail />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
