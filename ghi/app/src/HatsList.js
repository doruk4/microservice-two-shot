



function HatsList(props){
  console.log(props)
  const deleteHat = async (id) => {
    const hatUrl = `http://localhost:8090/hats/${id}`
    const fetchConfig = {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(hatUrl, fetchConfig)
    if (response.ok) {
        window.location.reload()
    }
  }
    return (

        <div>
        <h5>Your Hats</h5>
        <div className="d-flex flex-wrap">
          {props.hats.map(hat => (
            <div key={hat.id} className="col-lg-4 col-md-6">
              <div className="card" style={{ width: '12rem' }}>
                <a href= {`/hats/${hat.id}`} ><img src={hat.pict_url} className="card-img-top" alt="Hat" style={{ height: '180px', width: '100%', objectFit: 'cover' }} /></a>
                <div className="card-body">
                  <h5 className="card-title">{`${hat.color} ${hat.style_name}`}</h5>
                                <button onClick={(e) => deleteHat(hat.id)} >Delete</button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }


      export default HatsList;
