import React, {useEffect, useState} from 'react';

function HatForm() {

const handleSubmit = async (event) => {
  event.preventDefault();

  const data = {};
  data.fabric = fabric;
  data.style_name = style;
  data.color = color;
  data.pict_url = pict_url;
  data.location = location;
  console.log(data);

  const hatUrl = 'http://localhost:8090/hats/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await fetch(hatUrl, fetchConfig);
  if (response.ok) {
    const newHat = await response.json();
    console.log(newHat);
    setFabric('');
    setStyle('');
    setColor('');
    setPictUrl('');
    setLocation('');
      }
    }

  const [fabric, setFabric] = useState('')
  const handleFabricChange = (event) => {
      const value = event.target.value;
      setFabric(value);
    }
  const [style, setStyle] = useState('')
  const handleStyleChange = (event) => {
      const value = event.target.value;
      setStyle(value);
    }

  const [location, setLocation] = useState('')
  const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
      }

  const [color, setColor] = useState('')
  const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value);
    }

  const [pict_url, setPictUrl] = useState('')
  const handlePictUrlChange = (event) => {
    const value = event.target.value;
    setPictUrl(value);
  }

const [locations, setLocations] = useState([]);

const fetchData = async () => {
  const url = 'http://localhost:8100/api/locations/';

  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();

    setLocations(data.locations)

  }
}

useEffect(() => {
  fetchData();
}, []);


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
            <input onChange={handleFabricChange} value={fabric}
placeholder="fabric" required
       type="text" name="fabric" id="fabric"
       className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleChange} value={style} placeholder="style" required type="text" name="style" id="style" className="form-control"/>
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange={handleColorChange} value={color}placeholder="color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictUrlChange} value={pict_url} placeholder="picturl" required type="url" name="picturl" id="picturl" className="form-control"/>
              <label htmlFor="picturl">Picture URL</label>
            </div>
            <div className="mb-3">
            <select  onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
              <option  value="">Choose a location</option>
              {locations.map(location => {
                return (
                    <option key={location.closet_name} value={location.href}>
                    {location.closet_name}
                  </option>
                );
              })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}


export default HatForm;
